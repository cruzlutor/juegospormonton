from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import ListView, FormView
from apps.game.models import Game
from .forms import ContactForm
# Create your views here.
def index(request):
    return render(request, 'panel/index.html', {})

class GameList(ListView):
    model = Game
    template_name = 'panel/game_list.html'

class GameForm(FormView):
    form_class = ContactForm
    template_name = 'panel/game_form.html'

    def form_valid(self, form):
        print 'se esta guardando'
        return HttpResponse('result')