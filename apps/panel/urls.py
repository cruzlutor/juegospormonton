from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.index),
    url(r'^games/$', views.GameList.as_view()),
	url(r'^games/new/$', views.GameForm.as_view()),
)