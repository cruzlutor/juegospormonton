from django import forms
from apps.game.models import Game

class ContactForm(forms.ModelForm):
    class Meta:
        model = Game
        fields = [
            'plugin', 
            'wmode',
            'rating',
            'name',
            'slug',
            'categories',
            'description',
            'instructions',
            'flashvars',
            'width',
            'height',
            'is_featured',
            'is_published',
        ]

    def send_email(self):
        # send email using the self.cleaned_data dictionary
        pass