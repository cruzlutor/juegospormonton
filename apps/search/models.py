from django.db import models

class Search(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        app_label = 'search'
        db_table = 'search'

class SearchRequest(models.Model):
    search = models.ForeignKey('Search', related_name='request')
    add_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'search'
        db_table = 'search_request'