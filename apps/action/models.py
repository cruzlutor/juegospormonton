from django.db import models

class Action(models.Model):
    name = models.CharField(max_length=60)

    class Meta:
        app_label = 'action'
        db_table = 'action'
