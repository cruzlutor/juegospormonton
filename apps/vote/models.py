from django.db import models

class Vote(models.Model):
    name = models.CharField(max_length=60)

    class Meta:
        app_label = 'vote'
        db_table = 'vote'
