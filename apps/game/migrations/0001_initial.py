# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('vote', '0001_initial'),
        ('category', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plugin', models.IntegerField(max_length=1, choices=[(1, b'Flash'), (2, b'Shockwave'), (3, b'Unity'), (4, b'HTML')])),
                ('wmode', models.IntegerField(max_length=1, choices=[(1, b'Window'), (2, b'Direct'), (3, b'Opaque'), (4, b'Transparent'), (5, b'GPU')])),
                ('rating', models.IntegerField(max_length=1, choices=[(1, b'Everyone'), (2, b'Teens'), (3, b'Mature')])),
                ('name', models.CharField(max_length=60)),
                ('slug', models.CharField(max_length=60)),
                ('description', models.TextField()),
                ('instructions', models.TextField(null=True, blank=True)),
                ('width', models.IntegerField()),
                ('height', models.IntegerField()),
                ('is_featured', models.BooleanField(default=None)),
                ('is_published', models.BooleanField(default=None)),
                ('flashvars', models.TextField(null=True, blank=True)),
                ('add_date', models.DateTimeField(auto_now_add=True)),
                ('categories', models.ManyToManyField(related_name='games', to='category.Category')),
            ],
            options={
                'db_table': 'game',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GameControl',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=30)),
                ('game', models.ForeignKey(related_name='control', to='game.Game')),
            ],
            options={
                'db_table': 'game_control',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GameKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'game_key',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GamePlay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('add_date', models.DateTimeField(auto_now_add=True)),
                ('game', models.ForeignKey(related_name='plays', to='game.Game')),
                ('user', models.ForeignKey(related_name='plays', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'game_play',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GameSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.IntegerField(max_length=1, choices=[(1, b'File'), (2, b'Direct Load'), (3, b'Frame'), (4, b'External'), (5, b'Code'), (6, b'Link')])),
                ('platform', models.IntegerField(max_length=1, choices=[(1, b'Web'), (2, b'Mobile'), (3, b'Android'), (4, b'IOS'), (5, b'Windows Phone')])),
                ('source', models.TextField()),
                ('game', models.ForeignKey(related_name='source', to='game.Game')),
            ],
            options={
                'db_table': 'game_source',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GameVote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('add_date', models.DateTimeField(auto_now_add=True)),
                ('game', models.ForeignKey(related_name='votes', to='game.Game')),
                ('user', models.ForeignKey(related_name='votes', to=settings.AUTH_USER_MODEL)),
                ('vote', models.ForeignKey(to='vote.Vote')),
            ],
            options={
                'db_table': 'game_vote',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='gamecontrol',
            name='keys',
            field=models.ManyToManyField(to='game.GameKey'),
            preserve_default=True,
        ),
    ]
