from apps.category.models import Category
from apps.action.models import Action
from apps.vote.models import Vote

from django.contrib.auth.models import User
from django.db import models

GAME_PLATFORM = (
    (1, 'Web'),
    (2, 'Mobile'),
    (3, 'Android'),
    (4, 'IOS'),
    (5, 'Windows Phone'),
)

GAME_PLUGIN = (
    (1, 'Flash'),
    (2, 'Shockwave'),
    (3, 'Unity'),
    (4, 'HTML'),
)

GAME_WMODE = (
    (1, 'Window'),
    (2, 'Direct'),
    (3, 'Opaque'),
    (4, 'Transparent'),
    (5, 'GPU'),
)

GAME_SOURCE = (
    (1, 'File'),
    (2, 'Direct Load'),
    (3, 'Frame'),
    (4, 'External'),
    (5, 'Code'),
    (6, 'Link'),
)

GAME_RATING = (
    (1, 'Everyone'),
    (2, 'Teens'),
    (3, 'Mature'),
)

class Game(models.Model):
    plugin = models.IntegerField(max_length=1, choices=GAME_PLUGIN)
    wmode = models.IntegerField(max_length=1, choices=GAME_WMODE)
    rating = models.IntegerField(max_length=1, choices=GAME_RATING)
    name = models.CharField(max_length=60)
    slug = models.CharField(max_length=60)
    description = models.TextField()
    instructions = models.TextField(blank=True, null=True,)
    width = models.IntegerField()
    height = models.IntegerField()
    is_featured = models.BooleanField(default=None)
    is_published = models.BooleanField(default=None)
    flashvars = models.TextField(blank=True, null=True)
    add_date = models.DateTimeField(auto_now_add=True)
    categories = models.ManyToManyField(Category, related_name='games')

    class Meta:
        app_label = 'game'
        db_table = 'game'

class GameSource(models.Model):
    game = models.ForeignKey('Game', related_name='source')
    type = models.IntegerField(max_length=1, choices=GAME_SOURCE)
    platform = models.IntegerField(max_length=1, choices=GAME_PLATFORM)
    source = models.TextField()
    class Meta:
        app_label = 'game'
        db_table = 'game_source'

class GamePlay(models.Model):
    game = models.ForeignKey('Game', related_name='plays')
    user = models.ForeignKey(User, blank=True, null=True, related_name='plays')
    add_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'game'
        db_table = 'game_play'

class GameVote(models.Model):
    game = models.ForeignKey('Game', related_name='votes')
    user = models.ForeignKey(User, related_name='votes')
    vote = models.ForeignKey(Vote)
    add_date = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        app_label = 'game'
        db_table = 'game_vote'

class GameControl(models.Model):
    game = models.ForeignKey('Game', related_name='control')
    keys = models.ManyToManyField('GameKey')
    value = models.CharField(max_length=30)

    class Meta:
        app_label = 'game'
        db_table = 'game_control'

class GameKey(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        app_label = 'game'
        db_table = 'game_key'
