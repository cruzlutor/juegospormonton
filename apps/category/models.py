from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=60)
    title = models.CharField(max_length=60)
    slug = models.CharField(max_length=60)
    description = models.TextField(blank=True, null=True)
    is_principal = models.BooleanField(default=None)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children')
    
    class Meta:
        app_label = 'category'
        db_table = 'category'