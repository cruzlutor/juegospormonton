# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60)),
                ('title', models.CharField(max_length=60)),
                ('slug', models.CharField(max_length=60)),
                ('description', models.TextField(null=True, blank=True)),
                ('is_principal', models.BooleanField(default=None)),
                ('parent', models.ForeignKey(related_name='children', blank=True, to='category.Category', null=True)),
            ],
            options={
                'db_table': 'category',
            },
            bases=(models.Model,),
        ),
    ]
