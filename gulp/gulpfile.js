var gulp = require('gulp'),
    watch = require('gulp-watch'),
    stylus = require('gulp-stylus'),
    livereload = require('gulp-livereload');

gulp.task('style', function(){
    gulp.src([
        '../apps/**/*/*.styl',
        '!../apps/**/*/*.no.styl',
    ])
    .pipe(stylus())
    .pipe(gulp.dest('../apps/'))
    .pipe(livereload());
})

gulp.task('watch', function(){
    livereload.listen();
    gulp.watch('../apps/**/*/*.styl', ['style']);
    gulp.watch('../apps/**/*/*.html').on('change', livereload.changed);
    gulp.watch('../apps/**/*/*.js').on('change', livereload.changed);
})

gulp.task('dev', ['watch']);